package com.kccf.api.tblapi.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.kccf.api.tblapi.service.TblApiService;
import com.kccf.api.tblapi.service.TblApiVO;


@Path("/tblapi")
public class TblApiController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TblApiController.class);
	
	// tblApiService
	@Resource(name = "tblApiService")
	private TblApiService tblApiService;

	
	@GET
	@Path("/selectTblApi/{usr_no}")
	public String selectTblApi(@PathParam("usr_no") int usr_no) throws Exception
	{
		
		Map<String, Object> map = new HashMap<String, Object> ();
		
		TblApiVO apiVo = tblApiService.selectTblApi(usr_no);
		
		map.put("usr_no", apiVo.getUsr_no());
		map.put("name", apiVo.getName());
		map.put("content", apiVo.getContent());
		
		JSONObject obj = new JSONObject(map);
		return obj.toString();
	}
	
	@POST
	@Path("/insertTblApi")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertTblApi(@RequestBody String json) throws Exception
	{
		TblApiVO apiVo = new TblApiVO();
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(json);
		
		int usr_no = element.getAsJsonObject().get("usr_no").getAsInt();
		String name = element.getAsJsonObject().get("name").getAsString();
		String content = element.getAsJsonObject().get("content").getAsString();
		
		System.out.println("usr_no = "+usr_no);
		System.out.println("name = "+name);
		System.out.println("content = "+content);
		
		apiVo.setUsr_no(usr_no);
		apiVo.setName(name);
		apiVo.setContent(content);
		
		tblApiService.insertTblApi(apiVo);
		
		return Response.status(201).entity(json).build();
	}
}
