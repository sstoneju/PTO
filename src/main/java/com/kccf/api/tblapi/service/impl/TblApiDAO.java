package com.kccf.api.tblapi.service.impl;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.kccf.api.tblapi.service.TblApiVO;


@Repository("tblApiDAO")
public class TblApiDAO{

	@Inject
	private SqlSession sqlSession;
	
	public TblApiVO selectTblApi(int usr_no) throws Exception{
		return (TblApiVO) sqlSession.selectOne("TblApi.selectTblApi", usr_no);
	}
	
	

	public int insertTblApi(TblApiVO tblApiVo) throws Exception{
		return sqlSession.insert("TblApi.insertTblApi", tblApiVo);
	}
}