package com.kccf.api.tblapi.service;

public interface TblApiService {

	public TblApiVO selectTblApi(int usr_no) throws Exception;
	
	public String insertTblApi(TblApiVO apiVo) throws Exception;
}
