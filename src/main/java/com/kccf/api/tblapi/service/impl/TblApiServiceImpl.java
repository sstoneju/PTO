package com.kccf.api.tblapi.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.kccf.api.tblapi.service.TblApiService;
import com.kccf.api.tblapi.service.TblApiVO;

@Service ("tblApiService")
public class TblApiServiceImpl implements TblApiService{

	@Resource(name="tblApiDAO")
	private TblApiDAO tblApiDAO;
	
	
	public TblApiVO selectTblApi(int usr_no) throws Exception 
	{
		TblApiVO tblApiVO = tblApiDAO.selectTblApi(usr_no);
		return tblApiVO;
	}
	
	
	public String insertTblApi(TblApiVO apiVo) throws Exception
	{
		int resultCnt = tblApiDAO.insertTblApi(apiVo);
		return "" + resultCnt;
	}
}
