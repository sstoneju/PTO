package com.kccf.api.tblapi.service;

public class TblApiVO {
	int usr_no;
	String name;
	String content;
	
	public TblApiVO(int usr_no, String name, String content) {
		super();
		this.usr_no = usr_no;
		this.name = name;
		this.content = content;
	}
	
	public TblApiVO() {
		// TODO Auto-generated constructor stub
	}

	public int getUsr_no() {
		return usr_no;
	}
	public void setUsr_no(int usr_no) {
		this.usr_no = usr_no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public String toString() {
		return "TblApiVO [usr_no=" + usr_no + ", name=" + name + ", content="
				+ content + "]";
	}
	
	
}
