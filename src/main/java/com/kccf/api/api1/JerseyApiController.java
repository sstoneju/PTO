package com.kccf.api.api1;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.kccf.api.api1.model.ContentVO;



/**
 * Handles requests for the application home page.
 */

@Path("/message")
public class JerseyApiController {
	
	// url : /rest/message/test1
	// jersey 매핑 테스트 
	@GET
	@Path("/test1")
	public String getMsg()
	{
		return "Hello Jersey 2 !!";
	}
	
	// url : /rest/message/text
	// jersey 응답 테스트. output 포맷이 text_plain 방식
	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		return "Hello Jersey";
	}
	
	
	// url : /rest/message/xml
	// jersey 응답 테스트. output 포맷이 xml 방식
	@GET
	@Path("/xml")
	@Produces(MediaType.APPLICATION_XML)
	public ContentVO sayHelloXml() {
		ContentVO cntVO = new ContentVO(111, "name1", "content1");
		return cntVO;
	}
	
	// url : /rest/message/xml
	// jersey 응답 테스트. output 포맷이 json 방식
	@GET
	@Path("/json2")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayHelloJson() {
		ContentVO cntVO = new ContentVO(111, "name1", "content1");
		JSONObject json = new JSONObject(cntVO);
		return Response.status(200).entity(json).build();
		
	}
	
	// url : /rest/message/both
	// jersey 응답 테스트. output 포맷이 xml, json 방식 둘 다 지원.
	// 요청시 헤더에 accept 방식을 지정하면 해당 방식으로 response
	@GET
	@Path("/both")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public ContentVO sayHelloBoth() {
		ContentVO cntVO = new ContentVO(111, "name1", "content1");
		return cntVO;
	}
	
	
	
	
}
