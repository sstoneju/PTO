package com.kccf.api.api1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import com.kccf.api.api1.model.ContentVO;



/**
 * Handles requests for the application home page.
 */

@Path("/xml")
public class XmlSampleController {
	
	
	
	// url : /rest/xml/test2
	// xml 데이터를 받아서 그대로 응답
	// 입력데이터 : xml 데이터 아무거나 
	@POST
    @Path("/test2")
	@Consumes(MediaType.APPLICATION_XML)
    public Response postMsg(String msg) {
        String output = "POST:Jersey say : " + msg;
        return Response.status(200).entity(output).build();
    }
	
	// url : /rest/xml/test3/{id}
	// xml 데이터와 id 파라메터를 받아서 처리.
	// 입력데이터 : xml 아무거나.
	@POST
    @Path("/test3/{id}")
	@Consumes(MediaType.APPLICATION_XML)
    public Response postParam(@PathParam("id") long id, String msg) {
        String output = "POST:Jersey say : " + msg + "(" + id +")";
        
        return Response.status(200).entity(output).build();
    }
	
	// url : /rest/xml/test4
	// xml 데이터를 클래스로 매핑해서 받음
	// 입력데이터 : ContentVO에 맞는 xml 데이터 
	@POST
	@Path("/test4")
	@Consumes(MediaType.APPLICATION_XML)
	public Response post_xml(ContentVO test) {
		System.out.println("recv: " + test);
		
		return Response.status(200).entity(test).build();
	}
	
	
	// url : /rest/xml/test5
	// xml 데이터를 Map으로 매핑해서 받음
	// 입력데이터 : xml 데이터 아무거나  
	@POST
	@Path("/test5")
	@Consumes(MediaType.APPLICATION_XML)
	public Response post_jsonString(@RequestBody Map<String, Object> test) {
		System.out.println("recv: " + test);
		
		//System.out.println("id=" + test.get("id"));
		//System.out.println("cont=" + test.get("cont"));
		//System.out.println("name=" + test.get("name"));
		
		return Response.status(200).entity(test).build();
	}
	
	
	
}
