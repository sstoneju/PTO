package com.kccf.api.api1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;



/**
 * Handles requests for the application home page.
 */

@Path("/mysql")
public class MySqlSampleController {
	
	private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://192.168.25.12:3306/pst";
    private static final String USER = "admin";
    private static final String PW = "1234";
	
    // url : /rest/mysql/test1
 	// json을 받아서 String으로 전달 -> JsonParser로 파싱 .
	// map 방식 데이터를 json 으로 변환해서 응답.
	@GET
	@Path("/test1")
	public String getMsg()
	{
		// map -> json 변환
		Map<String, Object> map = new HashMap<String, Object> ();
		map.put("key1", "value1");
		map.put("key2", "value2");
		map.put("key3", "value3");
		List<String> list = new ArrayList<String>();
		list.add("list1");
		list.add("list2");
		list.add("list3");
		map.put("list", list);
		
		JSONObject obj = new JSONObject(map);
		return obj.toString();
	}
	

	// url : /rest/mysql/test2
	// mysql 동작테스트
 	// mysql db의 tbl_api 테이블 usr_no=2 의 데이터를 읽어서 json으로 변환 해서 봔환
	@GET
	@Path("/test2")
	public String test2() throws Exception
	{
		Class.forName(DRIVER);
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Map<String, Object> map = new HashMap<String, Object> ();
		
		try {
			conn =  DriverManager.getConnection(URL, USER, PW);
			pstmt = conn.prepareStatement(
					  "SELECT USR_NO, NAME, CONTENT FROM TBL_API WHERE USR_NO=?"); 
					                                   
			pstmt.setInt(1,2);       
			rs = pstmt.executeQuery();  
			
			while (rs.next()) {  
				map.put("select_usr_no", rs.getInt(1) );
				map.put("select_name", rs.getString(2));
				map.put("select_content", rs.getString(3));
			}
            
        } catch(Exception e) {
        	
            e.printStackTrace();
        } finally {
        	if(rs != null) {
        		try { rs.close(); } catch (Exception e) {};
        	}
        	if(conn != null) {
        		try { conn.close(); } catch (Exception e) {};
        	}
        }
		
		JSONObject obj = new JSONObject(map);
		return obj.toString();
	}
	

	
	// url : /rest/mysql/test2
	// mysql 동작테스트
	// mysql db의 tbl_api 테이블에 usr_no=4 인 데이터를 삽입 
	@POST
	@Path("/test3")
		public Response post_jsonString() throws Exception {
		
		Class.forName(DRIVER);
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		int usr_no = 4;
		String name = "name4";
		String content = "content4";
		

		try {
			conn =  DriverManager.getConnection(URL, USER, PW);
			pstmt = conn.prepareStatement(
					  "INSERT INTO TBL_API (USR_NO, NAME, CONTENT) VALUES (?, ?, ?)"); 
					                                   
			pstmt.setInt(1, usr_no);
			pstmt.setString(2, name);
			pstmt.setString(3, content);
			
			pstmt.executeUpdate();  
            
        } catch(Exception e) {
        	e.printStackTrace();
        	return Response.status(400).entity("Error").build();
        } finally {
        	if(pstmt != null) {
        		try { pstmt.close(); } catch (Exception e) {};
        	}
        	if(conn != null) {
        		try { conn.close(); } catch (Exception e) {};
        	}
        }
		
		return Response.status(201).entity("Inserted").build();
	}
	
	// url : /rest/mysql/post_json_string
	// json data를 받아서 tbl_api 테이블에 삽입
	// 입력 데이터 : {"usr_no" : 5,"name" : "name5","content" : "content5"}

	@POST
	@Path("/post_json_string")
	@Consumes(MediaType.APPLICATION_JSON) 
	public Response post_jsonString(@RequestBody String test) throws Exception {
		System.out.println("recv: " + test);
		
		Class.forName(DRIVER);
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(test);
		
		int usr_no = element.getAsJsonObject().get("usr_no").getAsInt();
		System.out.println("usr_no = "+usr_no);
		String name = element.getAsJsonObject().get("name").getAsString();
		System.out.println("name = "+name);
		String content = element.getAsJsonObject().get("content").getAsString();
		System.out.println("content = "+content);
		

		try {
			conn =  DriverManager.getConnection(URL, USER, PW);
			pstmt = conn.prepareStatement(
					  "INSERT INTO TBL_API (USR_NO, NAME, CONTENT) VALUES (?, ?, ?)"); 
					                                   
			pstmt.setInt(1, usr_no);
			pstmt.setString(2, name);
			pstmt.setString(3, content);
			
			pstmt.executeUpdate();  
            
        } catch(Exception e) {
        	e.printStackTrace();
        	//return Response.status(400).entity("Error").build();
        	return Response.status(400).entity(test).build();
        } finally {
        	if(pstmt != null) {
        		try { pstmt.close(); } catch (Exception e) {};
        	}
        	if(conn != null) {
        		try { conn.close(); } catch (Exception e) {};
        	}
        }
		
		return Response.status(201).entity(test).build();
	}
}
