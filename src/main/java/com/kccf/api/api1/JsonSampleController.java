package com.kccf.api.api1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.kccf.api.api1.model.ContentVO;



/**
 * Handles requests for the application home page.
 */

@Path("/json4")
public class JsonSampleController {
	
	
	// url : /rest/json4/test1
	// map을 json으로 변환해서 response 에 실어보냄.
	@GET
	@Path("/test1")
	public String getMsg()
	{
		// map -> json 변환
		Map<String, Object> map = new HashMap<String, Object> ();
		map.put("key1", "value1");
		map.put("key2", "value2");
		map.put("key3", "value3");
		List<String> list = new ArrayList<String>();
		list.add("list1");
		list.add("list2");
		list.add("list3");
		map.put("list", list);
		
		JSONObject obj = new JSONObject(map);
		return obj.toString();
	}
	
	
	// url : /rest/json4/test2
	// map을 json으로 변환해서 response 에 실어보냄.
	// json 데이터를 String으로 받고, 그대로 200 response 에 실어 보냄.
	@POST
    @Path("/test2")
	@Consumes(MediaType.APPLICATION_JSON)
    public Response postMsg(String msg) {
        String output = "POST:Jersey say : " + msg;
        return Response.status(200).entity(output).build();
    }
	
	// url : /rest/json4/test2
	// map을 json으로 변환해서 response 에 실어보냄.
	// json 데이터를 String으로 받고, 그대로 200 response 에 실어 보냄.
	@POST
    @Path("/test3/{id}")
    public Response postParam(@PathParam("id") long id, String msg) {
        String output = "POST:Jersey say : " + msg + "(" + id +")";
        
        return Response.status(200).entity(output).build();
    }
	
	// url : /rest/json4/post_json2
	// recv json as POJO class
	// json을 받아서 class로 매핑.
	@POST
	@Path("/post_json2")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post_json(ContentVO test) {
		System.out.println("recv: " + test);
	
		return Response.status(200).entity(test).build();
	}
	
	// url : /rest/json4/post_json_string3
	// json을 받아서 String으로 전달 -> JsonParser로 파싱 .
	@POST
	@Path("/post_json_string3")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post_jsonString(@RequestBody String test) {
		System.out.println("recv: " + test);
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(test);
		
		int usr_no = element.getAsJsonObject().get("usr_no").getAsInt();
		System.out.println("usr_no = "+usr_no);
		String name = element.getAsJsonObject().get("name").getAsString();
		System.out.println("name = "+name);
		String content = element.getAsJsonObject().get("content").getAsString();
		System.out.println("content = "+content);
		
		return Response.status(200).entity(test).build();
	}
	
	
}
