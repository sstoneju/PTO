package com.kccf.api.api1.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="content")
//public class ContentVO implements Serializable{
public class ContentVO {
    private long id;
    private String name;
    private String cont;
    
    public ContentVO() {
		super();
	}

	public ContentVO(long id, String name, String cont) {
        this.id = id;
        this.name = name;
        this.cont = cont;
    }

    public String getName() {
    	return name;
    }
    
    public long getId() {
        return id;
    }

    public String getCont() {
        return cont;
    }

    @XmlAttribute
	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	@Override
	public String toString() {
		return "ContentVO [id=" + id + ", name=" + name + ", cont=" + cont
				+ "]";
	}

}
