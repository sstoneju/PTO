package com.kccf.api.api1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kccf.api.api1.model.Greeting;

/**
 * Handles requests for the application home page.
 */
@RestController
@RequestMapping("/api")
public class ApiController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	private static final String template = "Hello, %s!";
	
	// url : /api/greeting
	// mapping 이 동작하는지 테스트
	@RequestMapping(value="/greeting" ,method=RequestMethod.GET)
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(111,String.format(template, name));
    }
	
	// url : /api/greeting/{id} 
	// url 방식 파라메터 테스트. id에 숫자 
	@RequestMapping(value = "/greeting/{id}", method = RequestMethod.GET)
    public String greeting_id(@PathVariable(value="id") String id) {
        return String.format(template, id);
    }
	
	// url : /api/greeting_param?id=1
	// parameter 방식 테스트
	@RequestMapping(value = "/greeting_param", params="id", method = RequestMethod.GET)
    public String greeting_param(@RequestParam(value="id") String id) {
        return String.format(template, id);
    }
	
	// url : /api/greeting_params?id=1&name="hong"
	// 멀티 파라메터 전달 테스트
	@RequestMapping(value = "/greeting_params", params={"id", "name"}, method = RequestMethod.GET)
    public String greeting_params(@RequestParam(value="id") String id) {
        return String.format(template, id);
    }
	
	// url : /api/greeting_response?id=1&name="hong"
	// response 방식 테스트. 특정 클래스 형식으로 response 전달
	@RequestMapping(value = "/greeting_response", params={"id", "name"}, method = RequestMethod.GET)
    public ResponseEntity<Greeting> greeting_response(@RequestParam(value="id") long id) {
        return new ResponseEntity<Greeting>(new Greeting(id, "test"), HttpStatus.OK);
    }
	
	// url : /api/greeting_response2?id=1&name="hong"
	// response 방식 테스트. 특정 클래스 형식으로 response 전달
	@RequestMapping(value = "/greeting_response2", params={"id", "name"}, method = RequestMethod.GET)
    public Greeting greeting_response2(@RequestParam(value="id") long id) {
        return new Greeting(id, "test");
    }
	
}
