package com.kccf.cmm.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;



public class GSONUtil {
	
	/*@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(GSONUtil.class);
	*/
	
	// Object -> json string
	public String objectToJson(Object obj) {
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		return json;
	}
	
	// Json -> Object example
	public void jsontoobject() {
		String json = "{\"name\":\"kim\",\"age\":20,\"gender\":\"M\"}";
		Gson gson = new Gson();
		
		/*
		Person person = gson.fromJson(json, Person.class);
		
		System.out.println("name = " + person.getName());
		System.out.println("age = " + person.getAge());
		System.out.println("gender = " + person.getGender());
		*/
		
	}
	
	
	
	// Make json example
	public void makeJson() {
		Gson gson = new Gson();
		JsonObject object = new JsonObject();
		
		object.addProperty("name", "park");
		object.addProperty("age", 22);
		object.addProperty("success", true);
		String json = gson.toJson(object);
		System.out.println(json);
	}
	
	// parse json example
	public void parsingJson() {
		String json = "{\"name\":\"kim\",\"age\":20,\"gender\":\"M\"}";
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(json);
		String name = element.getAsJsonObject().get("name").getAsString();
		System.out.println("name = "+name);
		int age = element.getAsJsonObject().get("age").getAsInt();
		System.out.println("age = "+age);
	}
	
	
	
	
}
